<%@ taglib prefix="z" tagdir="/WEB-INF/tags" %>
<z:mPage pageTitle="Index">
    <div class="row">
    <div class="col-md-8">
    <div class="jumbotron">
      <h1>CoolSlideShow</h1>
      <p>Make your photo slides now!</p>
         <button class="btn btn-primary btn-lg">Sign Up Today!</button></p>       
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-heading">Log In!</div>
        <div class="panel-body">
   <form class="form-signin">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
      
      </div>
    </div>
  </div>
</div>


</z:mPage>