<%@ taglib prefix="z" tagdir="/WEB-INF/tags" %>
<z:mPage pageTitle="Create CoolSlide">
   <ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#photos">Photos</a></li>
  <li><a data-toggle="tab" href="#text">Add Text</a></li>
  <li><a data-toggle="tab" href="#save">Save & Share</a></li>
</ul>

<div class="tab-content">
  <div id="photos" class="tab-pane fade in active">
    <div class="row">
     <div class="col-md-1"> <button class="btn btn-default">Upload pic from Computer</button>
     </div>
    </div>
  </div>
  <div id="text" class="tab-pane fade">
    <h3>Menu 1</h3>
    <p>Some content in menu 12.</p>
  </div>
  <div id="save" class="tab-pane fade">
    <h3>Menu 2</h3>
    <p>Some content in menu 2.</p>
  </div>
</div>

</z:mPage>